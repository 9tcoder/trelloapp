# TrelloApp #

###About It!###
TrelloApp is a web application used to interact with [Trello](https://trello.com/) which is used to organize stuff. Currently it only works for public boards which are visible to everyone. 
This app will use to determine time taken by cards to reach from initial list to current list.

### Technologies Used ###
It uses Grails 2.4.3 with JDK 1.8. For calling *Trello* APIs, it uses a Grails plugin : *Rest Client 1.0.0*.

### How to setup and run? ###
To run it quickly, system should have Grails 2.4.3 running. Get the source code from Bitbucket. Then run the commands :

***>grails clean;grails compile;grails run-app***

When it says :

***| Server running. Browse to http://localhost:8080/trelloApp***

Open the browser and hit the provided url.

### How to use app? ###

* Hit the URL on browser, it will display the home page where it will ask for Trello's board URL. You have to provide it in to the text box and press ***Go!***. For example, if url is ***https://trello.com/b/CMV0pT47/kanban*** then enter it like this :
![home.jpg](https://bitbucket.org/repo/Moyr6r/images/38226122-home.jpg)

* If URL is valid then it will display all lists exist in the board on next page. Else it will display the error message on same page. In below image, it displayed the three lists in drop down. You have to select one and then click to ***Go!***. 
![list.png](https://bitbucket.org/repo/Moyr6r/images/2357794167-list.png)

* Then it will display all cards exist in that list with time taken to reach from initial list to current list. Suppose if a card is created in list A and after five min. it has moved to list b. If you have selected the list B on previous screen then it will display card with message that it has moved from list A to current, 5 min. ago. 
Below image, shows that two cards are generated in same list and moved to different lists in past and again shifted to initial list. But last card has not moved to any list yet.  
![cards.jpg](https://bitbucket.org/repo/Moyr6r/images/3444244443-cards.jpg)