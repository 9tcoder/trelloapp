package org.trello.commandObject

import com.google.gson.annotations.SerializedName

import java.text.SimpleDateFormat

/**
 * Created by aman on 28/9/16.
 */
class CardAction {
    String id
    Data data
    String date

    Date parseDate(){
        def formatString = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

        def sdf = new SimpleDateFormat(formatString)
        sdf.setTimeZone(TimeZone.getTimeZone('UTC'));
        return sdf.parse(date)
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", data=" + data +
                ", date='" + date + '\'' +
                '}';
    }
}

