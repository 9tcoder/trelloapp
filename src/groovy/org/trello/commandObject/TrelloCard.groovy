package org.trello.commandObject

/**
 * Created by aman on 27/9/16.
 */
class TrelloCard {
    String id
    String name

    @Override
    public String toString() {
        return "TrelloCard{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
