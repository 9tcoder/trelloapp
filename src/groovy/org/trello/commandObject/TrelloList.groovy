package org.trello.commandObject
/**
 * Created by aman on 27/9/16.
 */
class TrelloList {

    String id
    String name


    @Override
    public String toString() {
        return "TrelloList{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
