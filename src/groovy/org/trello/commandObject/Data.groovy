package org.trello.commandObject

import com.google.gson.annotations.SerializedName

/**
 * Created by aman on 28/9/16.
 */
class Data {
    TrelloCard listAfter
    TrelloCard listBefore


    @Override
    public String toString() {
        return "{" +
                "listAfter=" + listAfter +
                ", listBefore=" + listBefore +
                '}';
    }
}
