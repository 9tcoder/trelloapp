package trello

import org.trello.commandObject.TrelloCard
import org.trello.commandObject.TrelloList

class PublicBoardController {
    def trelloRestService

    def index() {
        redirect(action: "board", params: params)
    }

    def board() {
    }

    def lists() {

        try {
            String boardURL = params.boardURL
            checkValidity(boardURL)

            def boardId = trelloRestService.getBoardId(boardURL)
            List<TrelloList> lists = trelloRestService.getList(boardId)
            render view: 'lists', model: [lists: lists, boardURL: boardURL]
            return
        } catch (Exception e) {
            flash.error = message(code: e.getMessage(), default: e.getMessage())
        }
        redirect(action: 'board', params: params)
    }

    def cards() {
        def listId = params.listId

        try {
            checkValidity(listId)
            List<TrelloCard> cards = trelloRestService.getCards(listId)

            if (cards?.size() < 1) throw new IllegalStateException("api.rest.result.no.card.in.list")

            Map result = trelloRestService.getCardHistory(cards)
            render view: 'cards', model: [result: result, listId: listId]
            return
        } catch (Exception e) {
            flash.error = message(code: e.getMessage(), default: e.getMessage())
        }
        redirect(action: 'lists', params: params)
    }

    private def checkValidity(String input) {
        if (!input || input.trim().isEmpty()) {
            throw new IllegalArgumentException("Please provide valid input : " + input)
        }
    }
}
