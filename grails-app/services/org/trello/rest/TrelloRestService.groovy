package org.trello.rest

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.transaction.Transactional
import groovy.time.TimeCategory
import groovy.time.TimeDuration
import org.trello.commandObject.CardAction
import org.trello.commandObject.TrelloCard
import org.trello.commandObject.TrelloList

@Transactional
class TrelloRestService {
    def messageSource
    def grailsApplication
    def rest = new RestBuilder()

    private String getURL(String apiMethod) {
        String BASE_URL = grailsApplication.config.trello.rest.api.url
        String KEY_VALUE = grailsApplication.config.trello.key
        return "${BASE_URL}${apiMethod}?key=${KEY_VALUE}"
    }

    public <T> T getResponse(String url, TypeToken<T> type) {
        RestResponse resp = callRestApi(url)

        if (!resp?.statusCode.equals(org.springframework.http.HttpStatus.OK)) {
            throw new IllegalStateException(
                    messageSource.getMessage("api.rest.result.not.valid", [resp?.responseEntity?.statusCode?.reasonPhrase, resp?.responseEntity?.statusCode].toArray(), Locale.ENGLISH))
        }

        Gson gson = new Gson();
        T objects = gson.fromJson(resp.json?.toString(), type.getType());
        return objects
    }

    private RestResponse callRestApi(String url) {
        def resp = rest.get(url)
        return resp
    }

    def getList(String boardId) {
        return this.<List<TrelloList>> getResponse(getURL("boards/${boardId}/lists"), new TypeToken<List<TrelloList>>() {})
    }

    def getCards(String listId) {
        return this.<List<TrelloCard>> getResponse(getURL("lists/${listId}/cards"), new TypeToken<List<TrelloCard>>() {})
    }

    def getCardActions(String cardId) {
        return this.<List<CardAction>> getResponse(getURL("cards/${cardId}/actions"), new TypeToken<List<CardAction>>() {})
    }

    def getCardHistory(List<TrelloCard> cards){
        def map = [:]
        cards.each {
            String result
            try {
                result = calculateTimePeriod(it.id)
            } catch (Exception e) {
                messageSource.setUseCodeAsDefaultMessage(true)
                result = messageSource.getMessage(e.getMessage(), [].toArray(), Locale.ENGLISH)
            }
            map.put(it.name, result)
        }
        return map
    }

    String calculateTimePeriod(String cardId){
        //Check card exists.
        TrelloCard card = this.<TrelloCard> getResponse(getURL("cards/${cardId}"), new TypeToken<TrelloCard>() {})
        if(!card) {
            throw new IllegalArgumentException("api.rest.result.card.id.invalid")
        }

        List<CardAction> cardActions = getCardActions(cardId)

        // Sort by DateTime
        cardActions = cardActions.sort{it.parseDate()}

        if(cardActions==null || cardActions.size()<1) throw new IllegalStateException("api.rest.result.card.initial.state")

        // Find initial list of the card. Check in first action.
        String sourceList = cardActions[0]?.data?.listBefore?.name

        // Time when card created. It is present in the card id.
        // Ref. from doc. It uses mongo Id. Convert first 8 HEX char to decimal. It will be unix timestamp.
        Date sourceCreationDate = new Date(Long.parseLong(cardId.substring(0,8), 16)*1000)

        // Time when card reached to current list
        Date destinationArrivalTime = cardActions?.last()?.parseDate()

        // Gap
        println destinationArrivalTime
        println sourceCreationDate
        TimeDuration gap = TimeCategory.minus(destinationArrivalTime, sourceCreationDate)

        return messageSource.getMessage("time.period.between.lists", [sourceList, gap].toArray(), Locale.ENGLISH)
    }

    def getBoardId(String url) {
        def SERVER_URL = grailsApplication.config.trello.board.url
        def init =  url.indexOf(SERVER_URL)
        if(init==-1) throw new IllegalArgumentException(
                messageSource.getMessage("input.url.not.valid", [url].toArray(), Locale.ENGLISH))
        int initialPosition = init+SERVER_URL.size()
        int endPosition = url.indexOf('/', initialPosition)
        if(endPosition==-1) throw new IllegalArgumentException(
                messageSource.getMessage("input.url.not.valid", [url].toArray(), Locale.ENGLISH))
        def boardId = url.substring(initialPosition, endPosition)
        if(boardId?.trim()?.isEmpty()) throw new IllegalArgumentException(
                messageSource.getMessage("input.url.invalid.boardId", [boardId].toArray(), Locale.ENGLISH))
        return boardId
    }
}
