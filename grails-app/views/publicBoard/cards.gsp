<%--
  Created by IntelliJ IDEA.
  User: aman
  Date: 27/9/16
  Time: 3:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
</head>

<body>

<g:form action="showCard">
    <ol style="list-style-type: none;">
        <div class="row">
            <li class="alert
             alert-info">
                <h1>Cards</h1>
            </li>
        </div>
        <g:each in="${result}" var="card">
            <div class="row">
                <li class="alert alert-success">
                    <h4>${card.key}</h4>
                    ${card.value}
                </li>
            </div>
        </g:each>
    </ol>

</g:form>
</body>
</html>