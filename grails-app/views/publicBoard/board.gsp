<%--
  Created by IntelliJ IDEA.
  User: aman
  Date: 27/9/16
  Time: 3:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">

</head>

<body>
    <g:form action="lists" class="form-horizontal">
        <div class="form-group form-group-lg">
            <div class="row offset1">
                <label for="boardURL"><g:message code="label.input.board"/></label>
                    <input type="text" name="boardURL" value="${params?.boardURL}" class="form-control">
                    <button class="btn btn-success" type="submit">Go!</button>
            </div><!-- /.row -->
        </div>
    </g:form>

</body>
</html>