<%--
  Created by IntelliJ IDEA.
  User: aman
  Date: 27/9/16
  Time: 3:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
</head>

<body>

    <g:form action="cards" class="form-inline">
        <g:hiddenField name="boardURL" value="${boardURL}"/>
        <div class="form-group form-group-lg">
                <label for="listId">
                    <g:message code="label.input.select.List"/>
                    <span class="required-indicator"> </span>
                </label>
                <g:select name='listId' value="${params.listId}"
                          from='${lists}'
                          optionKey="id" optionValue="name" class="form-control"></g:select>
                <button class="btn btn-success" type="submit">Go!</button>
        </div>
    </g:form>

</body>
</html>